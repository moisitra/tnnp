import numpy
from keras.datasets import cifar10
from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation
from keras.layers import Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils import np_utils

#set seed for reperability of learning outcomes
numpy.random.seed(42)

#load data and distributed across sets
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

#Pre-processing data

#normalize image pixel intensity data
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

#convert class mark to categories
Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

#create a convolution network

#create a model
model = Sequential() #layers go one after another

#a first convolution layer
model.add(Convolution2D(32, 3, 3, border_mode = 'same',
                         input_shape=(3, 32, 32),
                        activation='relu'))


#a second convolution layer
model.add(Convolution2D(32, 3, 3, activation = 'relu'))